using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TilesRoundHandler : MonoBehaviour
{
    [field: SerializeField] public int TileGroupsAmount { get; private set; } = 10;
    [field: SerializeField] public int TilesPerGroup { get; private set; } = 3;
    [SerializeField] private UnityEvent onTilesRemoved = null;

    public List<Tile> Tiles { get; } = new List<Tile>();

    public void CheckTilesCount()
    {
        if (Tiles.Count > 0) return;
        onTilesRemoved?.Invoke();
    }
}
