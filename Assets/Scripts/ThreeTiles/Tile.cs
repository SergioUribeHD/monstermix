using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [field: SerializeField] public int Id { get; private set; }
    [field: SerializeField] public SpriteRenderer SprRenderer { get; private set; }

    public static event System.Action<Tile> OnClick = null;


    private void OnMouseDown()
    {
        OnClick?.Invoke(this);
    }

    public bool Equals(Tile other)
    {
        return SprRenderer.sprite == other.SprRenderer.sprite;
    }
}
