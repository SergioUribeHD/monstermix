using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    [SerializeField] private TilesRoundHandler roundHandler = null;
    [SerializeField] private Tile[] tilePrefabs = new Tile[0];
    [SerializeField] private Transform center = null;
    [SerializeField] private Vector2 range = new Vector2(5, 3);

    private void Start()
    {
        List<int> usedIds = new List<int>();
        for (int i = 0; i < roundHandler.TileGroupsAmount; i++)
        {
            if (usedIds.Count == tilePrefabs.Length)
                usedIds.Clear();
            int id = Random.Range(0, tilePrefabs.Length);
            while (usedIds.Contains(id))
                id = Random.Range(0, tilePrefabs.Length);
            usedIds.Add(id);
            SpawnTileGroup(id);
        }
    }

    private void SpawnTileGroup(int id)
    {
        var tile = tilePrefabs[id];
        for (int i = 0; i < roundHandler.TilesPerGroup; i++)
            SpawnTile(tile, i);
    }

    private void SpawnTile(Tile tilePrefab, int index)
    {
        Vector2 randomPos = GetRandomPos();
        while (roundHandler.Tiles.TryFind(t => Vector2.Distance(t.transform.localPosition, randomPos) < 0.1f, out var t))
            randomPos = GetRandomPos();
        var tile = Instantiate(tilePrefab, randomPos + (Vector2)center.position, tilePrefab.transform.rotation, center);
        tile.name = $"Tile {tile.Id} - {index}";
        roundHandler.Tiles.Add(tile);
    }

    private Vector2 GetRandomPos()
    {
        Vector2 randomPos = new Vector2(Random.Range(-range.x, range.x), Random.Range(-range.y, range.y));
        randomPos.x = Mathf.RoundToInt(randomPos.x);
        randomPos.y = Mathf.RoundToInt(randomPos.y);
        return randomPos;
    }

}
