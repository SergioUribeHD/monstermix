using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TilesRack : MonoBehaviour
{
    [SerializeField] private TilesRoundHandler roundHandler = null;
    [SerializeField] private int maxTilesCapacity = 8;
    [SerializeField] private Transform startingTilePoint = null;
    [SerializeField] private UnityEvent onMaxCapacityReached = null;

    public bool MaxCapacityReached => tilesHeld.Count == maxTilesCapacity;

    private List<Tile> tilesHeld = new List<Tile>();

    private void Awake()
    {
        Tile.OnClick += AddTile;
    }

    private void OnDestroy()
    {
        Tile.OnClick -= AddTile;
    }

    private void AddTile(Tile tile)
    {
        if (MaxCapacityReached) return;
        Vector2 tilePos = new Vector2(startingTilePoint.position.x + tilesHeld.Count, startingTilePoint.position.y);
        tile.transform.position = tilePos;
        tile.transform.SetParent(startingTilePoint);
        tilesHeld.Add(tile);
        roundHandler.Tiles.Remove(tile);
        CheckTilesRow();
        CheckCapacity();
    }

    private void CheckTilesRow()
    {
        foreach (var tile in tilesHeld)
        {
            var matchingTiles = tilesHeld.FindAll(t => t.Equals(tile));
            if (matchingTiles.Count == roundHandler.TilesPerGroup)
            {
                DestroyAll(tile, matchingTiles);
                for (int i = 0; i < tilesHeld.Count; i++)
                    tilesHeld[i].transform.position = new Vector2(startingTilePoint.position.x + i, startingTilePoint.position.y);
                break;
            }
        }
    }

    private void DestroyAll(Tile match, List<Tile> tiles)
    {
        tilesHeld.RemoveAll(t => t.Equals(match));
        roundHandler.CheckTilesCount();
        foreach (var tile in tiles)
            Destroy(tile.gameObject);
    }

    private void CheckCapacity()
    {
        if (!MaxCapacityReached) return;
        onMaxCapacityReached?.Invoke();
    }
}
