﻿using System.Collections.Generic;

public static class ListExtensions
{

    /// <summary>
    /// Returns a random item
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="remove">If true, removes the returned item from the list.</param>
    /// <returns></returns>
    public static T GetRandomItem<T>(this List<T> list, bool remove)
    {
        T item = list[UnityEngine.Random.Range(0, list.Count)];
        if (remove)
            list.Remove(item);
        return item;
    }

    /// <summary>
    /// Returns a random item and removes it from the list
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <returns></returns>
    public static T GetRandomItem<T>(this List<T> list) => list.GetRandomItem(false);

    public static T GetLastItem<T>(this List<T> list) => list[list.Count - 1];

    public static T RemoveLastItem<T>(this List<T> list)
    {
        var item = list.GetLastItem();
        list.Remove(item);
        return item;
    }

    public static bool TryFind<T>(this List<T> list, System.Predicate<T> match, out T item)
    {
        item = list.Find(match);
        return item != null;
    }
}
